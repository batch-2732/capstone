class Product {
  // done
  constructor(name, price) {
    // done
    this.name = name;
    this.price = price;
    this.isActive = true;
    console.log(`product ${this.name} created`);
  }
  archive() {
    // done
    this.isActive = false;
    console.log(`product ${this.name} is archived`);
  }
  updatePrice(newPrice) {
    // done
    this.price = newPrice;
    console.log(`price of product ${this.name} is updated`);
  }
}
class CartContent {
  // done
  constructor(product, quantity) {
    this.product = product;
    this.quantity = quantity;
  }
}
class Cart {
  constructor() {
    // done
    this.contents = [];
    this.totalAmount = 0;
  }
  addToCart(product, quantity) {
    // done
    this.contents.push(new CartContent(product, quantity));
    this.computeTotal();
  }
  showCartContents() {
    // done
    console.log("here are the contents of your cart");
    this.contents.forEach((content) => {
      console.log(content);
    });
  }
  updateProductQuantity(name, newQuantity) {
    //done
    this.contents.forEach((content) => {
      if ((content.product.name = name)) {
        content.quantity = newQuantity;
        console.log("product amount updated");
      }
    });
  }
  clearCartContents() {
    // done
    this.contents = [];
    this.totalAmount = 0;
    console.log("cart contents cleared");
  }
  computeTotal() {
    // done
    let runningTotal = 0;
    this.contents.forEach((content) => {
      runningTotal = runningTotal + content.quantity * content.product.price;
    });
    this.totalAmount = runningTotal;
    console.log(`totalAmount in cart is ${this.totalAmount}`);
  }
}
class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }
  checkOut() {
    // done
    if (this.cart.contents.length == 0) {
      console.log("cannot check out empty cart ");
    } else {
      this.orders.push(this.cart);
      this.cart = new Cart();
      console.log("cart is checked out");
    }
  }
}

const john = new Customer("john@mail.com");
console.log(john);
const prodA = new Product("soap", 9.99);
console.log(prodA);
prodA.updatePrice(12.99);
console.log(prodA);
prodA.archive();
console.log(prodA);
console.log("adding 3 soaps to cart");
john.cart.addToCart(prodA, 3);
john.cart.showCartContents();
console.log("changing soap quantity to 5");
john.cart.updateProductQuantity("soap", 5);
john.cart.showCartContents();
john.cart.computeTotal();
console.log("clearing cart contents");
john.cart.clearCartContents();
john.cart.showCartContents();
john.cart.computeTotal();
console.log(john.cart);
console.log("checking out");
john.checkOut();
console.log("adding 5 soaps to cart");
john.cart.addToCart(prodA, 5);
console.log("checking out again");
john.checkOut();
console.log(john);
